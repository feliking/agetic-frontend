# CRUD DE PEDIDOS Y PRODUCTOS

## Descripción

Frontend del sistema

## Requerimientos del sistema

- NodeJS v16

## Instalación y configuración

Dentro de la carpeta del proyecto, instalar las dependencias con:
```bash
$ npm install
```

Configurar la ruta API para axios dentro de la carpeta ./src/plugins/axios.js ("axios.defaults.baseURL = 'http://localhost:3000/' // Ruta API del servidor")

Por último levantar el servidor de pruebas de vue con:
```bash
$ npm run serve
```
